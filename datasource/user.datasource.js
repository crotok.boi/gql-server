const { RESTDataSource } = require("apollo-datasource-rest");

class User extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = "http://localhost:3000";
  }

  async getUser(id) {
    const user = await this.get(`/user/${id}`);
    const contents = await this.get(`/content/findAllByUser/${id}`);
    
    var result = {
      _id: user._id,
      email: user.email,
      password: user.password,
      contents: contents,
    }
    return result;
  }
}

module.exports = User;
const { ApolloServer, gql } = require('apollo-server');
const User = require('./datasource/user.datasource')

const typeDefs = gql`
type User {
  email: String,
  password: String,
  contents: [Content]
}

type Content {
  title: String,
  content: String,
}

type Query {
    getUser (id: String): User
  }
`;

const resolvers = {
  Query: {
    getUser: async (_, { id }, { dataSources }) =>
      dataSources.User.getUser(id)
  }
};

const server = new ApolloServer({
  typeDefs,
  resolvers,
  dataSources: () => ({
    User: new User()
  })
});

server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});